module.exports = {
  'env': {
    'browser': true,
    'node': true,
  },
  'extends': [
    'plugin:vue/recommended',
    'eslint:all',
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module',
  },
  'root': true,
  'rules': {
    'array-element-newline': [
      'error',
      {
        'ArrayExpression': {
          'minItems': 1,
        },
        'ArrayPattern': {
          'minItems': 2,
        },
      },
    ],
    'arrow-body-style': [
      'error',
      'always',
    ],
    'capitalized-comments': 'off',
    'comma-dangle': [
      'error',
      'always-multiline',
    ],
    'function-call-argument-newline': [
      'error',
      'consistent',
    ],
    'function-paren-newline': [
      'error',
      'multiline-arguments',
    ],
    'id-length': 'off',
    'indent': [
      'error',
      2,
      {
        'SwitchCase': 1,
      },
    ],
    'init-declarations': 'off',
    'linebreak-style': 'off',
    'lines-around-comment': 'off',
    'max-len': [
      'error',
      {
        'code': 100,
        'ignoreComments': true,
      },
    ],
    'max-lines': 'off',
    'max-lines-per-function': 'off',
    'max-statements': 'off',
    'multiline-comment-style': 'off',
    'multiline-ternary': [
      'error',
      'always-multiline',
    ],
    'newline-per-chained-call': 'off',
    'no-console': 'off',
    'no-continue': 'off',
    'no-extra-parens': [
      'error',
      'all',
      {
        'nestedBinaryExpressions': false,
      },
    ],
    'no-magic-numbers': 'off',
    'no-multi-spaces': 'error',
    'no-new': 'off',
    'no-plusplus': 'off',
    'no-ternary': 'off',
    'no-warning-comments': 'off',
    'object-curly-newline': [
      'error',
      {
        'minProperties': 1,
      },
    ],
    'object-curly-spacing': [
      'error',
      'always',
    ],
    'one-var': [
      'error',
      'never',
    ],
    'padded-blocks': [
      'error',
      'never',
      {
        'allowSingleLineBlocks': false,
      },
    ],
    'quotes': [
      'error',
      'single',
    ],
    'semi': [
      'error',
      'never',
    ],
    'sort-keys': [
      'error',
      'asc',
      {
        'allowLineSeparatedGroups': true,
        'natural': true,
      },
    ],
    'vue/html-comment-indent': [
      'error',
      2,
    ],
    'vue/html-indent': [
      'error',
      2,
    ],
    'vue/html-self-closing': 'off',
    'vue/multi-word-component-names': 'off',
    'vue/script-indent': [
      'error',
      2,
      {
        'baseIndent': 0,
        'switchCase': 1,
      },
    ],
  },
}
