/*
 * @Author: 18059301505 275895816@qq.com
 * @Date: 2022-11-07 15:48:40
 * @LastEditors: 18059301505 275895816@qq.com
 * @LastEditTime: 2022-11-29 12:02:16
 * @FilePath: \bosssoft-train-user-permission-centre-front-end\src\api\right\role.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/utils/request'

const ROLE_URL = '/v1/role'

export const getUserResource = (params) => {
  return request({
    url: ROLE_URL + '/queryUserResource',
    method: 'get',
    params,
  })
}

export const add = (data) => {
  return request({
    url: ROLE_URL,
    method: 'post',
    data,
  })
}

export const remove = (id, version) => {
  return request({
    url: ROLE_URL,
    method: 'delete',
    params: {
      id,
      version,
    },
  })
}

export const batchRemove = (data) => {
  return request({
    url: ROLE_URL + '/batch',
    method: 'delete',
    data,
  })
}

export const modify = (data) => {
  return request({
    url: ROLE_URL,
    method: 'put',
    data,
  })
}

export const copy = (id) => {
  return request({
    url: ROLE_URL + '/copy',
    method: 'post',
    params: { id },
  })
}

export const commonQuery = (params) => {
  return request({
    url: ROLE_URL,
    method: 'get',
    params,
  })
}

export const queryRoleById = (id) => {
  return request({
    url: ROLE_URL + '/' + id,
    method: 'get',
  })
}

export const commonQueryUser = (params) => {
  return request({
    url: ROLE_URL + '/commonQueryUser',
    method: 'get',
    params,
  })
}

export const assignUser = (data) => {
  return request({
    url: ROLE_URL + '/assignUser',
    method: 'post',
    data,
  })
}

export const assignResource = (data) => {
  return request({
    url: ROLE_URL + '/assignResource',
    method: 'post',
    data,
  })
}

export const selectCompany = () => {
  return request({
    url: ROLE_URL + '/selectCompany',
    method: 'get',
  })
}

export const queryRoleCompany = (id) => {
  return request({
    url: ROLE_URL + '/company',
    method: 'get',
    params: { id },
  })
}

export const modifyRoleCompany = (data) => {
  return request({
    url: ROLE_URL + '/company',
    method: 'put',
    data,
  })
}
